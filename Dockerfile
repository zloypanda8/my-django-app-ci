FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN apt update && apt install -y git

RUN git clone https://gitlab.com/chumkaska1/django_blog.git

WORKDIR /django_blog
RUN cp ?sgi.py ./Blog
RUN pip install --upgrade pip && pip install -r requirements.txt

